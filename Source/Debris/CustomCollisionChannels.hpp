#pragma once

#include "Engine/EngineTypes.h"

constexpr inline auto ECC_Player = ECC_GameTraceChannel1;
constexpr inline auto ECC_Projectile = ECC_GameTraceChannel2;
constexpr inline auto ECC_TriggerArea = ECC_GameTraceChannel3;
