// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DebrisGameModeBase.generated.h"

UCLASS()
class DEBRIS_API ADebrisGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
