// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

DECLARE_DELEGATE(FProjectileDestroyed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FProjectileVelocityChanged, FVector, NewVelocity);

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Combat))
class DEBRIS_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	AProjectile();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetVelocity(FVector Velocity);

	UFUNCTION(BlueprintCallable)
	void SetDamage(const UDamageType* Type, int Amount);

	bool IsInViewport() const;

	FProjectileDestroyed OnDestroyed;

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly)
	FProjectileVelocityChanged OnVelocityChange;

	virtual void Destroyed() override
	{
		(void)OnDestroyed.ExecuteIfBound();
	}

protected:
	virtual void BeginPlay() override;
	
private:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta=(AllowPrivateAccess))
	USphereComponent* Collision;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta=(AllowPrivateAccess))
	const UDamageType* DamageType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess))
	int DamageAmount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess))
	FVector Velocity;
};
