// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

#include "DrawDebugHelpers.h"

#include "Debris/CustomCollisionChannels.hpp"

AProjectile::AProjectile()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	Collision->SetSimulatePhysics(true);
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Collision->SetCollisionObjectType(ECC_PhysicsBody);
	Collision->SetEnableGravity(false);
	FCollisionResponseContainer C;
	C.SetAllChannels(ECR_Block);
	C.SetResponse(ECC_Camera, ECR_Ignore);
	C.SetResponse(ECC_Pawn, ECR_Overlap);
	C.SetResponse(ECC_WorldDynamic, ECR_Overlap);
	C.SetResponse(ECC_Vehicle, ECR_Overlap);
	C.SetResponse(ECC_Player, ECR_Overlap);
	C.SetResponse(ECC_TriggerArea, ECR_Overlap);
	Collision->SetCollisionResponseToChannels(C);

	Collision->SetLinearDamping(0.0f);
	Collision->SetHiddenInGame(false);

	RootComponent = Collision;
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	Collision->SetPhysicsLinearVelocity(Velocity);
}

void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Collision->AddForce(Velocity, NAME_None, true);
	
	if(!IsInViewport())
	{
		Destroy();
	}
}

bool AProjectile::IsInViewport() const
{
	FVector Origin, Extent;
	GetActorBounds(true, Origin, Extent);
	
	const APlayerController* const PlayerController = GetWorld()->GetFirstPlayerController();
	
	FVector2D ViewportSize;
	GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);
	const FBox2D Viewport {FVector2D::ZeroVector, ViewportSize};

	// loop through bounding box and check if any point is in the viewport
	for (int i = 0; i < 4; i++)
	{
		const int SignX = (i % 2) * 2 - 1; // -1 or 1 depending on i
		const int SignZ = (2 * i - 3) / abs(2 * i - 3); // -1 or 1 depending on i
		FVector2D ScreenCoords;
		const auto WorldLocation = Origin + SignX * Extent.X + SignZ * Extent.Z;
		PlayerController->ProjectWorldLocationToScreen(WorldLocation, ScreenCoords);

		if (Viewport.IsInside(ScreenCoords))
		{
			return true;
		}
	}
	return false;
}
	
void AProjectile::SetVelocity(FVector NewVelocity)
{
	Velocity = NewVelocity;
	Collision->SetPhysicsLinearVelocity(Velocity);
	OnVelocityChange.Broadcast(Velocity);
}

void AProjectile::SetDamage(const UDamageType* Type, int Amount)
{
	DamageType = Type;
	DamageAmount = Amount;
}
