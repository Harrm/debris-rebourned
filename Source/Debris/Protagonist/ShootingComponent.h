// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "Components/ActorComponent.h"
#include "ShootingComponent.generated.h"


UCLASS(ClassGroup=(Combat), meta=(BlueprintSpawnableComponent))
class DEBRIS_API UShootingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UShootingComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Shooting")
	void Shoot(FVector Direction, float Speed);
	
protected:
	virtual void BeginPlay() override;

private:
	TOptional<AProjectile*> SpawnProjectile();
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta=(AllowPrivateAccess))
	TSubclassOf<AProjectile> ProjectileType;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta=(AllowPrivateAccess))
	FVector ProjectileOriginOffset;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta=(AllowPrivateAccess, UIMin=1, UIMax=10, ClampMax=30, ClampMin=1))
	int ProjectilePoolSize = 1;

	int ActiveProjectilesCount = 0;
};
