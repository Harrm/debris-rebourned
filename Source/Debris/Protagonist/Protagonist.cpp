﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Protagonist.h"

#include <algorithm>

#include "DrawDebugHelpers.h"
#include "InputState.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Datasmith/DatasmithCore/Public/DatasmithDefinitions.h"
#include "Materials/MaterialInstanceDynamic.h"

AProtagonist::AProtagonist()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	Collision->SetSimulatePhysics(true);
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Collision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	Collision->BodyInstance.SetMaxAngularVelocityInRadians(0, false, true);
	Collision->BodyInstance.bLockYTranslation = true;
	Collision->SetEnableGravity(true);
	RootComponent = Collision;

	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));
	CameraArm->bInheritPitch = false;
	CameraArm->bInheritRoll = false;
	CameraArm->bInheritYaw = false;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));

	ShadowDecal = CreateDefaultSubobject<UDecalComponent>(TEXT("ShadowDecal"));
	ShadowDecal->SetFadeScreenSize(0.0);
	ShadowDecal->DecalSize = FVector{1.f, 1.f, 1.f} * 64.f;
	ShadowDecal->SetRelativeRotation(FRotator{90.f, 0.f, 0.f});

	Shooting = CreateDefaultSubobject<UShootingComponent>(TEXT("Shooting"));

	CameraArm->SetupAttachment(Collision);
	Camera->SetupAttachment(CameraArm);
	Mesh->SetupAttachment(Collision);
	ShadowDecal->SetupAttachment(Collision);

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	SightDirection = FVector::ForwardVector;
}

void AProtagonist::BeginPlay()
{
	Super::BeginPlay();
}

void AProtagonist::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	auto* DynamicInstance = UMaterialInstanceDynamic::Create(ShadowDecal->GetDecalMaterial(), ShadowDecal);
	ShadowDecal->SetDecalMaterial(DynamicInstance);
}

void AProtagonist::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Collision->SetAllPhysicsAngularVelocityInDegrees(FVector::ZeroVector);
	Collision->SetWorldRotation(FRotator::ZeroRotator);

	HasFloorBelow = CheckFloorBelow();

	if (!HasFloorBelow)
	{
		if (State == EState::Idle || State == EState::Walking)
		{
			State.Set<EState::Falling>();
		}
	} else
	{
		if (State == EState::Falling)
		{
			ResetState();
		}
	}

	auto NewWallInfo = CheckWallsNearby();
	HasWallNearby = NewWallInfo.IsSet();

	if (GetState() != EState::StuckToWall)
	{
		SecSinceUnstuckFromWall += DeltaTime;
		if (!GetIsDead() && IsInAir() && HasWallNearby && WantsToStickToWall(NewWallInfo->TouchPoint))
		{
			StickToWall(NewWallInfo.GetValue());
		}
	}
	else if (!IsInAir() || !HasWallNearby || !WantsToStickToWall(LastWallInfo.TouchPoint))
	{
		UnstickFromWall();
	}
	else
	{
		auto& Data = State.GetData<EState::StuckToWall>();
		Data.Duration += DeltaTime;
	}

	if (!IsInAir() && !IsAiming() && WantsToAim)
	{
		StartAiming();
	}
	UpdateSightDirection();

	if (IsAiming())
	{
		UpdateAim();
	}

	if (IsShooting)
	{
		SecsSinceLastShot += DeltaTime;
	}

	if (State != EState::Dead && IsShooting && SecsSinceLastShot > AutoFireDelayInSecs)
	{
		Shoot();
	}

	UpdateVelocity(DeltaTime);
	UpdateMeshFacing();
	UpdateCollisionExtent();
}

void AProtagonist::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AProtagonist::StartJump).bExecuteWhenPaused =
		true;
	InputComponent->BindAction("Jump", EInputEvent::IE_Released, this, &AProtagonist::StopJump).bExecuteWhenPaused =
		true;

	InputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &AProtagonist::StartShooting).bExecuteWhenPaused
		= true;
	InputComponent->BindAction("Shoot", EInputEvent::IE_Released, this, &AProtagonist::StopShooting).bExecuteWhenPaused
		= true;

	InputComponent->BindAction("LookUp", EInputEvent::IE_Pressed, this, &AProtagonist::LookUp).bExecuteWhenPaused =
		true;
	InputComponent->BindAction("LookUp", EInputEvent::IE_Released, this, &AProtagonist::StopLookingUp).
	                bExecuteWhenPaused = true;

	InputComponent->BindAction("LookDown", EInputEvent::IE_Pressed, this, &AProtagonist::LookDown).bExecuteWhenPaused =
		true;
	InputComponent->BindAction("LookDown", EInputEvent::IE_Released, this, &AProtagonist::StopLookingDown).
	                bExecuteWhenPaused = true;

	InputComponent->BindAction("Aim", EInputEvent::IE_Pressed, this, &AProtagonist::StartAiming).bExecuteWhenPaused =
		true;
	InputComponent->BindAction("Aim", EInputEvent::IE_Released, this, &AProtagonist::StopAiming).bExecuteWhenPaused =
		true;

	InputComponent->BindAxis("AimX").bExecuteWhenPaused = true;
	InputComponent->BindAxis("AimY").bExecuteWhenPaused = true;
	InputComponent->BindVectorAxis(EKeys::Mouse2D).bExecuteWhenPaused = true;

	InputComponent->BindAxis("MoveRight");
}

bool AProtagonist::IsInAir() const
{
	return !HasFloorBelow;
}

static float TraceDistanceToFloor(UWorld* World, const AActor* Ignore, FVector Origin, float Limit) {
	FHitResult OnHit;
	FCollisionQueryParams Params{TEXT("Protagonist Floor Trace"), false, Ignore};
	FCollisionObjectQueryParams TargetParams(ECC_TO_BITFIELD(ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_WorldDynamic));
	bool AboveFloor = World->LineTraceSingleByObjectType(OnHit, Origin,
															  Origin + FVector::DownVector * Limit,
															  TargetParams, Params);
	bool TouchesFloor = false;
	if (AboveFloor && OnHit.bBlockingHit)
	{
		return OnHit.Distance;
	}
	return FLT_MAX;
}

bool AProtagonist::CheckFloorBelow() const
{
	// to make character stand in its blueprint viewport
#if WITH_EDITOR
	if(!GetWorld()->HasBegunPlay()) return true;
#endif
	
	auto OnFloorDistance = Collision->GetCollisionShape().GetExtent().Z
		+ FallTraceThreshold;

	// small adjustment to avoid detecting slightly penetrated walls
	auto ActorWidth = FVector::ForwardVector * Collision->GetScaledBoxExtent().X - 1.0f; 
	auto DistanceCenter = TraceDistanceToFloor(GetWorld(), this, GetActorLocation(), 10000);
	auto DistanceLeft = TraceDistanceToFloor(GetWorld(), this, GetActorLocation() + ActorWidth, 10000);
	auto DistanceRight = TraceDistanceToFloor(GetWorld(), this, GetActorLocation() - ActorWidth, 10000);

	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + FVector::DownVector * DistanceCenter, FColor::Cyan, false, -1.f,
	  255);
	DrawDebugLine(GetWorld(), GetActorLocation() + ActorWidth, GetActorLocation() + ActorWidth + FVector::DownVector * DistanceLeft, FColor::Cyan, false, -1.f,
	  255);
	DrawDebugLine(GetWorld(), GetActorLocation() - ActorWidth, GetActorLocation() - ActorWidth + FVector::DownVector * DistanceRight, FColor::Cyan, false, -1.f,
	  255);
	
	auto MinDistance = FMath::Min3(DistanceCenter, DistanceRight, DistanceLeft);
	
	ShadowDecal->SetWorldLocation(GetActorLocation() + FVector::DownVector * DistanceCenter, false, nullptr, ETeleportType::ResetPhysics);
	auto* DynamicInstance = Cast<UMaterialInstanceDynamic>(ShadowDecal->GetDecalMaterial());
	check(DynamicInstance != nullptr);
	DynamicInstance->SetScalarParameterValue("Density", FMath::Lerp(2.5, 0.0, DistanceCenter / 1000.f));
	float R = FMath::Lerp(0.5f, 0.0f, DistanceCenter / 1000.f);
	DynamicInstance->SetScalarParameterValue("Radius", R);

	bool TouchesFloor = MinDistance < OnFloorDistance;

	FColor Color;
	if (TouchesFloor)
	{
		Color = FColor::Red;
	}
	else
	{
		Color = FColor::Green;
	}

	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + FVector::DownVector * OnFloorDistance, Color, false, -1.f,
		  255);
	
	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + SightDirection * 80.0f, FColor::Blue, false, -1.f,
	              254);

	return TouchesFloor;
}

namespace
{
	TOptional<FHitResult> CheckWallPresence(UWorld* World, FVector Origin, FVector Probe)
	{
		FHitResult OnHit;
		FCollisionQueryParams Params{TEXT("Protagonist Wall Check Trace"), false};
		FCollisionObjectQueryParams TargetParams(ECC_TO_BITFIELD(ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_WorldDynamic));
		bool TouchesWall = World->LineTraceSingleByObjectType(OnHit, Origin,
		                                                      Origin + Probe,
		                                                      TargetParams, Params);
		if (TouchesWall && OnHit.bBlockingHit)
		{
			return OnHit;
		}
		return {};
	}
}

auto AProtagonist::CheckWallsNearby() const -> TOptional<FWallInfo>
{
	const auto RightProbe = FVector::ForwardVector * (Collision->GetScaledBoxExtent().X + 5.0f);
	const auto RightWall = CheckWallPresence(GetWorld(), GetActorLocation(), RightProbe);
	if (RightWall.IsSet())
	{
		return FWallInfo{RightWall->ImpactNormal, RightWall->ImpactPoint};
	}
	const auto LeftProbe = -RightProbe;
	const auto LeftWall = CheckWallPresence(GetWorld(), GetActorLocation(), LeftProbe);
	if (LeftWall.IsSet())
	{
		return FWallInfo{LeftWall->ImpactNormal, LeftWall->ImpactPoint};
	}
	return {};
}

FVector AProtagonist::GetVelocity() const
{
	return Collision->GetPhysicsLinearVelocity();
}

void AProtagonist::StartJump()
{
	bool WallJump = State == EState::StuckToWall || (HasWallNearby && SecSinceUnstuckFromWall < DurationToJumpAfterUnstuckFromWall);
	bool FloorJump = HasFloorBelow && CanMove();
	if (WallJump)
	{
		UE_LOG(LogTemp, Display, TEXT("Wall Jump"));
		auto Direction = FRotator{WallJumpAngle * FMath::Sign(-LastWallInfo.Normal.X), 0.0f, 0.0f}.RotateVector(LastWallInfo.Normal);
		Collision->AddImpulse(Direction * InitialWallJumpVelocity, NAME_None, true);
	}
	else if (FloorJump)
	{
		UE_LOG(LogTemp, Display, TEXT("Floor Jump"));
		State.Set<EState::Jumping>();
		Collision->AddImpulse(FVector::UpVector * InitialJumpVelocity, NAME_None, true);
	}
}

void AProtagonist::StopJump()
{
	if (State != EState::Jumping) return;
	UE_LOG(LogTemp, Display, TEXT("Jump Stop"));
	ResetState();
	auto V = GetVelocity();
	V.Z = FMath::Min(0.0f, V.Z);
	SetVelocity(V);
}

void AProtagonist::LookUp()
{
	WantsToLookUp = true;
}

void AProtagonist::StopLookingUp()
{
	WantsToLookUp = false;
}

void AProtagonist::LookDown()
{
	WantsToLookDown = true;
}

void AProtagonist::StopLookingDown()
{
	WantsToLookDown = false;
}

void AProtagonist::StartShooting()
{
	if (State == EState::Dead || IsShooting) return;
	Shoot();
	IsShooting = true;
}

void AProtagonist::StopShooting()
{
	IsShooting = false;
}

void AProtagonist::Shoot()
{
	SecsSinceLastShot = 0.0f;
	if (SightDirection.SizeSquared() > 0.0)
	{
		Shooting->Shoot(SightDirection, ProjectileSpeed);
	}
	else
	{
		Shooting->Shoot(LookingForward ? FVector::ForwardVector : FVector::BackwardVector, ProjectileSpeed);
	}
}

void AProtagonist::ApplyForce(FVector const& Force, float DeltaTime)
{
	if (Collision->IsSimulatingPhysics())
	{
		Collision->AddForce(Force, NAME_None, true);
	}
	else
	{
		Collision->AddWorldOffset(Force * DeltaTime, false, nullptr, ETeleportType::ResetPhysics);
	}
}

void AProtagonist::UpdateAim()
{
	auto AimAxis = FVector{InputComponent->GetAxisValue("AimX"), 0.0, InputComponent->GetAxisValue("AimY")};
	if (AimAxis.IsNearlyZero())
	{
		FVector2D MousePos;
		bool Res = GetWorld()->GetGameViewport()->GetMousePosition(MousePos);
		FVector2D ViewportSize;
		GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);
		if (Res)
		{
			auto Origin = Collision->GetComponentLocation();
			FVector2D Screen;
			check(GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(Origin, Screen));
			AimAxis.X = Screen.X - MousePos.X;
			AimAxis.Z = Screen.Y - MousePos.Y;
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Neither mouse nor gamepad is attached"));
		}
	}
	AimAxis.Normalize();

	SightDirection = AimAxis;
}

void AProtagonist::UpdateVelocity(float DeltaTime)
{
	UpdateHorizontalVelocity(DeltaTime);
	UpdateVerticalVelocity(DeltaTime);
}

void AProtagonist::UpdateVerticalVelocity(float DeltaTime)
{
	// maximum falling down speed is zero by default if we're not falling or gliding
	float NegativeVelocityLimit = 0.0f;

	switch (State.GetState())
	{
	case EState::Falling:
		{
			NegativeVelocityLimit = -FallingMaxSpeed;
			auto& Data = State.GetData<EState::Falling>();
			float Duration = Data.Duration += DeltaTime;
			ApplyForce(FVector::DownVector * FallingAcceleration.GetFloatValue(Duration), DeltaTime);
			break;
		}
	case EState::Jumping:
		{
			auto& Data = State.GetData<EState::Jumping>();
			Data.Duration += DeltaTime;

			ApplyForce(FVector::UpVector * JumpUpAcceleration.GetFloatValue(Data.Duration), DeltaTime);
			// if we are already jumping for too long, stop 
			if (Data.Duration > JumpUpAcceleration.GetTimeRange().Max)
			{
				StopJump();
			}
			break;
		}
	case EState::StuckToWall:
		{
			auto& Data = State.GetData<EState::StuckToWall>();
			if (Data.Duration > DurationToWallSlideStart)
			{
				NegativeVelocityLimit = -WallSlidingSpeed;
				SetVelocity(FVector::DownVector * WallSlidingSpeed);
			}
			break;
		}
	default:
		break;
	}

	auto V = GetVelocity();
	if (V.Z < NegativeVelocityLimit)
	{
		V.Z = NegativeVelocityLimit;
		SetVelocity(V);
	} else if (V.Z > MaxVerticalSpeed)
	{
		V.Z = MaxVerticalSpeed;
		SetVelocity(V);
	}
}

void AProtagonist::UpdateHorizontalVelocity(const float DeltaTime)
{
	if (State == EState::StuckToWall)
	{
		return;
	}
	FVector V = GetVelocity();
	// Process player input
	if (const auto HMovement = InputComponent->GetAxisValue("MoveRight");
		FMath::Abs(HMovement) > FLT_EPSILON && CanMove())
	{
		if (State == EState::Idle)
		{
			ResetState();
		}
		ApplyForce(FVector::ForwardVector * -HMovement * RunAcceleration, DeltaTime);
		if (FMath::Abs(V.X) > RunMaxSpeed)
		{
			V.X = FMath::Sign(V.X) * RunMaxSpeed;
			SetVelocity(V);
		}
	}
	else if (V.X != 0.0f)
	{
		// Stop if velocity is too low
		if (FMath::Abs(V.X) < StopHorizontalVelocity)
		{
			V.X = 0.0f;
			SetVelocity(V);
			if (State == EState::Walking)
			{
				ResetState();
			}
		}
		else
		{
			ApplyForce(FVector::BackwardVector * FMath::Sign(GetVelocity().X) * RunDeceleration, DeltaTime);
		}
	}
}

void AProtagonist::UpdateMeshFacing()
{
	auto VX = GetVelocity().X;
	if (VX < -StopHorizontalVelocity && LookingForward)
	{
		LookingForward = false;
		Mesh->SetRelativeRotation(FRotator(0, 90.0, 0.0));
	}
	else if (VX > StopHorizontalVelocity && !LookingForward)
	{
		LookingForward = true;
		Mesh->SetRelativeRotation(FRotator(0, -90.0, 0.0));
	}
}

void AProtagonist::Die_Implementation()
{
	State.Set<EState::Dead>();
	SetVelocity(FVector::ZeroVector);
	Collision->SetSimulatePhysics(false);
	Collision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh->SetSimulatePhysics(true);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

void AProtagonist::Resurrect_Implementation()
{
	State.Set<EState::Idle>();
	Mesh->SetSimulatePhysics(false);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Collision->SetSimulatePhysics(true);
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->AttachToComponent(Collision, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	Mesh->SetRelativeLocation(FVector{0.f, 0.f, -93.f}, false, nullptr, ETeleportType::ResetPhysics);
	Mesh->SetRelativeRotation(FRotator{0.f, -90.f, 0.0f});
	SightDirection = FVector::ForwardVector;
	LookingForward = true;
}

void AProtagonist::StartAiming()
{
	WantsToAim = true;
	if (CanAim())
	{
		Cast<APlayerController>(GetController())->SetShowMouseCursor(true);
		State.Set<EState::Aiming>();
	}
}

void AProtagonist::StopAiming()
{
	Cast<APlayerController>(GetController())->SetShowMouseCursor(false);
	WantsToAim = false;
	UpdateSightDirection();
	ResetState();
}

bool AProtagonist::CanMove() const
{
	return !IsAiming() && State != EState::StuckToWall && State != EState::Dead;
}

bool AProtagonist::CanAim() const
{
	return State != EState::Dead && !IsInAir() && State != EState::StuckToWall;
}

void AProtagonist::UpdateSightDirection()
{
	if (IsAiming()) return; // controlled by the player directly

	if (State == EState::StuckToWall)
	{
		SightDirection = LastWallInfo.Normal;
		return;
	}
	const bool CanLookDown = WantsToLookDown && IsInAir();

	if (WantsToLookUp && !CanLookDown) SightDirection.Z = 1.0f;
	else if (!WantsToLookUp && CanLookDown) SightDirection.Z = -1.0f;
	else SightDirection.Z = 0.0f;

	const auto HMovement = InputComponent->GetAxisValue("MoveRight");

	if (FMath::IsNearlyZero(HMovement))
	{
		if (FMath::IsNearlyZero(SightDirection.Z))
		{
			SightDirection.X = LookingForward ? 1.0f : -1.0f;
		}
		else
		{
			SightDirection.X = 0.0f;
		}
	}
	else
	{
		SightDirection.X = -FMath::Sign(HMovement);
	}
}

bool AProtagonist::IsNearWall() const
{
	return HasWallNearby;
}

bool AProtagonist::WantsToStickToWall(FVector WallLocation) const
{
	const auto HMovement = InputComponent->GetAxisValue("MoveRight");
	const auto Direction = (GetActorLocation() - WallLocation).X;
	return FMath::Abs(HMovement) > FLT_EPSILON && FMath::Sign(HMovement) == FMath::Sign(Direction);
}

bool AProtagonist::IsSlidingDownWall() const
{
	if (State == EState::StuckToWall)
	{
		auto& Data = State.GetData<EState::StuckToWall>();
		return Data.Duration > DurationToWallSlideStart;
	}
	return false;
}

float AProtagonist::GetSinceLastUnstuckFromWallDuration() const
{
	return SecSinceUnstuckFromWall;
}

void AProtagonist::StickToWall(FWallInfo NewWallInfo)
{
	SecSinceUnstuckFromWall = 0.0f;
	
	State.Set<EState::StuckToWall>();
	LastWallInfo = NewWallInfo;
	SetVelocity(FVector::ZeroVector);
}

void AProtagonist::UnstickFromWall()
{
	SecSinceUnstuckFromWall = 0.0f;
	ResetState();
}

void AProtagonist::ResetState()
{
	if (State == EState::Dead) return;

	if (!HasFloorBelow)
	{
		// four states fit: jumping, sticking to wall, falling and gliding
		// can't become jumping or sticking to wall passively
		State.Set<EState::Falling>();
	}
	else if (WantsToAim)
	{
		State.Set<EState::Aiming>();
		StartAiming();
	}
	else
	{
		const auto HMovement = InputComponent->GetAxisValue("MoveRight");
		if (FMath::Abs(HMovement) > FLT_EPSILON)
		{
			State.Set<EState::Walking>();
		}
		else
		{
			State.Set<EState::Idle>();
		}
	}
}

void AProtagonist::SetVelocity(FVector NewVelocity)
{
	Collision->SetAllPhysicsLinearVelocity(NewVelocity);
}

void AProtagonist::StartFloating_Implementation()
{
	switch(State.GetState())
	{
	case EState::Walking:
	case EState::Idle:
	case EState::Falling:
		State.Set<EState::Floating>();
		break;
	case EState::Jumping: break;
	case EState::Floating: break;
	case EState::Aiming: break;
	case EState::StuckToWall: break;
	case EState::Dead: break;
	}
}

void AProtagonist::StopFloating_Implementation()
{
	if(State == EState::Floating) ResetState();
}

void AProtagonist::UpdateCollisionExtent()
{
	auto DefaultExtent = GetDefault<AProtagonist>(this->GetClass())->Collision->GetUnscaledBoxExtent();
	
	switch(State.GetState())
	{
	case EState::Walking:
	case EState::Idle:
	case EState::Aiming:
	case EState::Dead:
		Collision->SetBoxExtent(DefaultExtent);
		break;

	case EState::Jumping: 
	case EState::Floating: 
	case EState::Falling:
		DefaultExtent.Z *= InAirColliderHeight;
		Collision->SetBoxExtent(DefaultExtent);
		break;
	
	case EState::StuckToWall:
		DefaultExtent.Z *= WallStickColliderHeight;
		Collision->SetBoxExtent(DefaultExtent);
		break;
	}
}
