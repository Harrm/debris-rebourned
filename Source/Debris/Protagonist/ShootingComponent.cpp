// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootingComponent.h"

#include "Debris/Protagonist/Projectile.h"

UShootingComponent::UShootingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	ProjectileType = AProjectile::StaticClass();
	ProjectileOriginOffset = FVector::ZeroVector;
}

void UShootingComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void UShootingComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                       FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UShootingComponent::Shoot_Implementation(const FVector Direction, const float Speed)
{
	auto P = SpawnProjectile();
	if(!P.IsSet()) return;
	
	if(auto* Projectile = P.GetValue(); IsValid(Projectile))
	{
		Projectile->SetVelocity(Direction * Speed);
		Projectile->OnDestroyed.BindLambda([this]()
		{
			if (IsValid(this))
			{
				ActiveProjectilesCount--;
			}
		});
	}
}

TOptional<AProjectile*> UShootingComponent::SpawnProjectile()
{
	if (ActiveProjectilesCount < ProjectilePoolSize)
	{
		const auto Origin = ProjectileOriginOffset + GetOwner()->GetActorLocation();
		auto* P = Cast<AProjectile>(GetWorld()->SpawnActor(ProjectileType.Get(), &Origin, &FRotator::ZeroRotator));
		ActiveProjectilesCount++;
		return P;
	}
	return {};
}
