﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Templates/IsInvocable.h"

#include "ShootingComponent.h"
#include "Components/BoxComponent.h"

#include "Protagonist.generated.h"

class UCameraComponent;
class UCapsuleComponent;
class UInputComponent;

struct FWallInfo
{
	FVector Normal;
	FVector TouchPoint;
};

UENUM(Category="Protagonist", BlueprintType)
enum class EState: uint8
{
	Walking,
	Idle,
	Jumping,
	Falling,
	Floating,
	Aiming,
	StuckToWall,
	Dead
};

struct FStatelessState
{
};

struct FFallingStateData
{
	float Duration = 0.0f;
};

struct FJumpingStateData
{
	float Duration = 0.0f;
};

struct FWallStickStateData
{
	TOptional<FWallInfo> WallInfo;
	float Duration = 0.0f;
};

template <EState State>
struct TSelectStateDataType;

#define MAP_STATE_DATA(State, DataType) \
template <> \
struct TSelectStateDataType<EState::State> \
{ \
using Type = DataType; \
};

MAP_STATE_DATA(Jumping, FJumpingStateData);
MAP_STATE_DATA(Idle, FStatelessState);
MAP_STATE_DATA(Walking, FStatelessState);
MAP_STATE_DATA(Falling, FFallingStateData);
MAP_STATE_DATA(Floating, FFallingStateData);
MAP_STATE_DATA(Aiming, FStatelessState);
MAP_STATE_DATA(StuckToWall, FWallStickStateData);
MAP_STATE_DATA(Dead, FStatelessState);

using FStateData = TVariant<FStatelessState, FFallingStateData, FJumpingStateData, FWallStickStateData>;

struct FProtagonistState
{
	EState GetState() const
	{
		return State;
	}

	bool operator==(EState S) const
	{
		return State == S;
	}

	bool operator!=(EState S) const
	{
		return State != S;
	}

	template <EState State, typename... TArgs>
	void Set(TArgs&&... Args)
	{
		this->State = State;
		StateData.Emplace<TSelectStateDataType<State>::Type>(std::forward<TArgs>(Args)...);
		UE_LOG(LogTemp, Display, TEXT("New state: %s"), *UEnum::GetValueAsString(State));
	}

	template <EState State>
	typename TSelectStateDataType<State>::Type& GetData()
	{
		check(State == this->State);
		return StateData.Get<typename TSelectStateDataType<State>::Type>();
	}

	template <EState State>
	const typename TSelectStateDataType<State>::Type& GetData() const
	{
		check(State == this->State);
		return StateData.Get<typename TSelectStateDataType<State>::Type>();
	}

	template <typename F, typename... Fs>
	decltype(auto) Visit(const F& f, const Fs&... fs)
	{
		// 'default' case
		if constexpr (TIsInvocable<F, FStateData&>::Value)
		{
			return f(StateData);
		}
		else
		{
#define CASE_STATE(StateName) \
case EState::StateName: \
if constexpr (TIsInvocable<F, TSelectStateDataType<EState::StateName>::Type&>::Value) \
{ \
return f(GetData<EState::StateName>()); \
} else \
{ \
return Visit(fs...); \
}
			switch (State)
			{
			CASE_STATE(Aiming);
			CASE_STATE(Jumping);
			CASE_STATE(Idle);
			CASE_STATE(Walking);
			CASE_STATE(Falling);
			CASE_STATE(Floating);
			CASE_STATE(StuckToWall);
			CASE_STATE(Dead);
			}
			checkNoEntry();
			__assume(0);
		}
#undef CASE_STATE
	}

	template <typename F>
	auto Visit(const F& f)
	{
		static_assert(TIsInvocable<F, FStateData&>::Value, "'Default' case must be processed");
		return f(StateData);
	}

	template <typename T>
	static T DefaultStateCase(FStateData&)
	{
		if constexpr (!TIsVoidType<T>::Value)
		{
			return {};
		}
	}

private:
	EState State = EState::Idle;

	FStateData StateData;
};

USTRUCT(BlueprintType)
struct FScaledCurve
{
	GENERATED_BODY()

	FScaledCurve() noexcept
	{
	}

	FScaledCurve(UCurveFloat* Curve, float Scale) noexcept
	{
		ensureAlwaysMsgf(Curve != nullptr, TEXT("Null Curve passed"));
		this->Curve = Curve;
		this->Scale = Scale;
	}

	auto GetFloatValue(const float DeltaTime) const
	{
		check(Curve != nullptr);
		return Curve->GetFloatValue(DeltaTime) * Scale;
	}

	struct FTimeRange
	{
		float Min, Max;
	};

	FTimeRange GetTimeRange() const
	{
		check(Curve != nullptr);
		FTimeRange t;
		Curve->GetTimeRange(t.Min, t.Max);
		return t;
	}

private:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess))
	UCurveFloat* Curve{};
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess))
	float Scale = 1.0f;
};

UCLASS(ClassGroup=(Protagonist))
class DEBRIS_API AProtagonist : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AProtagonist();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category="Movement")
	bool IsInAir() const;

	UFUNCTION(BlueprintCallable, Category="Lifecycle")
	bool GetIsDead() const
	{
		return GetState() == EState::Dead;
	}

	UFUNCTION(BlueprintCallable, Category="Lifecycle")
	EState GetState() const
	{
		return State.GetState();
	}

	// Apply a force to the protagonist for one tick duration
	UFUNCTION(BlueprintCallable, Category="Movement")
	void ApplyForce(FVector const& Force, float DeltaTime);

	virtual FVector GetVelocity() const override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Lifecycle")
	void Die();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Lifecycle")
	void Resurrect();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Movement")
	void StartFloating();
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Movement")
	void StopFloating();
	
private:
	bool CheckFloorBelow() const;
	TOptional<FWallInfo> CheckWallsNearby() const;

	void StartJump();
	void StopJump();
	void LookUp();
	void StopLookingUp();
	void LookDown();
	void StopLookingDown();

	void StartShooting();
	void StopShooting();
	void Shoot();

	void StartAiming();
	void StopAiming();

	void UpdateAim();

	void UpdateVelocity(float DeltaTime);
	void UpdateVerticalVelocity(float DeltaTime);
	void UpdateHorizontalVelocity(float DeltaTime);
	void UpdateMeshFacing();

	void UpdateCollisionExtent();
	
	bool CanMove() const;
	bool CanAim() const;

	bool IsAiming() const { return WantsToAim && CanAim(); }

	void UpdateSightDirection();

	void StickToWall(FWallInfo NewWallInfo);
	void UnstickFromWall();

	bool IsNearWall() const;
	bool WantsToStickToWall(FVector WallLocation) const;
	bool IsSlidingDownWall() const;

	float GetSinceLastUnstuckFromWallDuration() const;

	void ResetState();

	void SetVelocity(FVector NewVelocity);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Combat|Shoot", meta=(AllowPrivateAccess))
	float ProjectileSpeed = 1000.f;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category="Combat|Shoot", meta=(AllowPrivateAccess))
	FVector SightDirection = FVector::ForwardVector;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Combat|Shoot",
		meta=(AllowPrivateAccess, UIMin=0.1, UIMax=3, ClampMax=5, ClampMin=0.1))
	float AutoFireDelayInSecs = 1.0f;

	FProtagonistState State;

	FWallInfo LastWallInfo;

	bool LookingForward = true;

	bool WantsToAim = false;
	bool WantsToLookUp = false;
	bool WantsToLookDown = false;
	bool WantsToGlide = false;
	bool IsShooting = false;

	bool HasFloorBelow = false;

	float SecsSinceLastShot = FLT_MAX;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, meta=(AllowPrivateAccess))
	float SecSinceUnstuckFromWall = FLT_MAX;

	// Velocity threshold, reaching lower than which the character stops moving horizontally axis 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Run", meta=(AllowPrivateAccess))
	float StopHorizontalVelocity = 0.005f;

	// Maximum distance between the character and the ground, after which the character is considered falling 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement", meta=(AllowPrivateAccess))
	float FallTraceThreshold = 5.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Run", meta=(AllowPrivateAccess))
	float RunAcceleration = 1.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Run", meta=(AllowPrivateAccess))
	float RunDeceleration = 1.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Run", meta=(AllowPrivateAccess))
	float RunMaxSpeed = 200.0f;

	// Vertical acceleration applied up when the character is jumping up 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Jump", meta=(AllowPrivateAccess))
	FScaledCurve JumpUpAcceleration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Jump", meta=(AllowPrivateAccess))
	float InitialJumpVelocity = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Jump", meta=(AllowPrivateAccess))
	float InitialWallJumpVelocity = 100.0f;

	// Vertical acceleration applied down when the character is falling
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Fall", meta=(AllowPrivateAccess))
	FScaledCurve FallingAcceleration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|Fall", meta=(AllowPrivateAccess))
	float FallingMaxSpeed = 1000.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement", meta=(AllowPrivateAccess, ClampMax=1.0f, ClampMin=0.1f, UIMax=1.0f, UIMin=0.1f))
	float InAirColliderHeight = 0.8f;

	// Horizontal deceleration applied when in air
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement", meta=(AllowPrivateAccess))
	float InAirHorizontalDrag = 200.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|WallSticking", meta=(AllowPrivateAccess))
	float DurationToWallSlideStart = 1.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|WallSticking", meta=(AllowPrivateAccess))
	float DurationToJumpAfterUnstuckFromWall = 0.3f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|WallSticking", meta=(AllowPrivateAccess))
	float WallSlidingSpeed = 200.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|WallSticking", meta=(AllowPrivateAccess, ClampMax=1.0f, ClampMin=0.1f, UIMax=1.0f, UIMin=0.1f))
	float WallStickColliderHeight = 0.5f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement|WallSticking",
		meta=(AllowPrivateAccess, ClampMax=90.0f, ClampMin=-90.0f, ToolTip=
			"Angle between the wall normal and the jump direction in degrees.", UIMax=60.0f, UIMin=-60.0f))
	float WallJumpAngle = -45.0f;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, meta=(AllowPrivateAccess))
	bool HasWallNearby = false;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(AllowPrivateAccess))
	USkeletalMeshComponent* Mesh;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(AllowPrivateAccess))
	UBoxComponent* Collision;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(AllowPrivateAccess))
	UCameraComponent* Camera;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(AllowPrivateAccess))
	UDecalComponent* ShadowDecal;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(AllowPrivateAccess))
	UShootingComponent* Shooting;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(AllowPrivateAccess))
	USpringArmComponent* CameraArm;
	
	constexpr static float MaxVerticalSpeed = 10000.0f;
};
