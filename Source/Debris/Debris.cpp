// Copyright Epic Games, Inc. All Rights Reserved.

#include "Debris.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Debris, "Debris" );
